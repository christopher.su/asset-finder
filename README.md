# Asset Finder

## To Run Locally use following commands
* Run `yarn` to install dependencies.
* Run `yarn build:watch` to start webpack in watch mode.

## JSON Data Structure

### Illustration
```json
{
  // Name
    "name": "No Results from Filter/Search",
  // Description
    "description": "Use when no results are returned from filtering or searching through a table.",
  // Tags to parse when text searching
    "tags": ["filter", "table", "data"],
  // Use case for filters
    "useCase": "table",
  // Product usage
    "products": "All Products",
  // "Best Match" filter level, 
    "level":1,
  // SVG equivelent
    "svg": "no_result",
  // Actual content
    "info": [
      {
      // name
        "type": "Illustration",
      // content for illustrations: componentID
        "content": "a31f422a4666f2326dcbc7c2e8b722a83d5a3870",
      // content type
        "var": "Component"
      },
      {
      // name
        "type": "Title Text",
      // content for text: text
        "content": "No results found",
      // content type
        "var": "String"
      }
    ]
  }
```

### Text
```json
{
  // Name
    "name": "Country List",
  // Description
    "description": "Contains countries within the dropdown",
  // Tags to parse when text searching
    "tags": ["filter", "table", "data"],
  // Product Filter
    "products": ["all"],
  // "Best Match" sort
    "level": 3,
  // Top Level Swap (swap all content) 1: true, 0:false
    "swap": 1,
  // actual content
    "info": [
      {
      // Top level swap: 1: true, 0: false
        "swap": 0,
      // title
        "title": "Australia",
      // nested content
        "items": [
          {
          // type
            "type": "text",
          // content
            "content": "Australia"
          },
          {
          // type
            "type": "svg",
          // content
            "content": "australia"
          }
        ]
      }
    ]
  }
```

### Tooling
This repo is using following:
* [Figma Plugin React Template](https://github.com/nirsky/figma-plugin-react-template)
* React + Webpack
* TypeScript
* TSLint
* Prettier precommit hook
